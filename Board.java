public class Board
{
  private Die die1;
  private Die die2;
  private boolean[] closedTiles;
  
  public Board()
  {
    this.die1 = new Die();
    this.die2 = new Die();
    closedTiles = new boolean[12];
  }
  
  public String toString()
  {
    String tiles = "";
    for (int i = 0; i < 12; i++)
    {
      if (closedTiles[i])
      {
        tiles += "x ";
      }
      else
      {
        tiles += (i+1)+" ";
      }
    }
    return tiles;
  }
  
  public boolean playATurn()
  {
    boolean alreadyClosed = false;
    die1.roll();
    die2.roll();
    System.out.println("you roled" + die1.toString() + " and" + die2.toString());
    int sum = die1.getpips() + die2.getpips();
    if (!closedTiles[sum-1])
    {
      closedTiles[sum-1]= true;
      System.out.println("closing tile");
    }
    else
    {
      alreadyClosed = true;
      System.out.println("tile already Closed");
    }
    return alreadyClosed;
  }
}