public class ShutTheBox
{
	final static java.util.Scanner scan = new java.util.Scanner (System.in);
	public static void main(String[]args)
	{
		System.out.println("welcome to this *game*");
		Board board = new Board();
		boolean playSessionOver = false;
		int Player1Wins = 0, Player2Wins = 0;
		while(!playSessionOver)
		{
			boolean gameOver = false;
			while(!gameOver)
			{
				System.out.println("\nPlayer's 1 turn\n" + board.toString());
				if (board.playATurn())
				{
					System.out.println("Player 2 has won the game\n");
					gameOver= true;
					Player2Wins++;
				}
				else
				{
					System.out.println("\nPlayer's 2 turn\n" + board.toString());
					if (board.playATurn())
				{
					System.out.println("Player 1 has won the game\n");
					gameOver= true;
					Player1Wins++;
				}
				}
				
			}
			System.out.println("Player 1 has won "+ Player1Wins+" times\nPlayer 2 has won "+Player2Wins+" times");
			System.out.println("would you like to play again yes or no");
			char givingUp = Character.toLowerCase(scan.nextLine().charAt(0));
			if(givingUp=='n')
			{
				playSessionOver = true;
			}
		}
		
	}
}