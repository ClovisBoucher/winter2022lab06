import java.util.Random;
public class Die
{
  private int pips;
  private Random newroll;
  
  public Die()
  {
    this.pips = 1;
    this.newroll = new Random();
  }
  
  public int getpips()
  {
    return this.pips;
  }
  
  public void roll()
  {
// random number 1 =< and <7
    this.pips = newroll.nextInt(1,7);
  }
  
  public String toString()
  {
    String rollValue = " "+this.pips;
    return rollValue;
  }
}